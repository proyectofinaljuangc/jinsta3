package com.juang.jinsta2

import android.app.Application
import com.google.firebase.auth.FirebaseUser

class JinstasApplication : Application() {
    companion object {
        const val PATH_JINSTAS = "jinstas"
        const val PROPERTY_LIKE_LIST = "likeList"

        lateinit var currentUser: FirebaseUser
    }
}