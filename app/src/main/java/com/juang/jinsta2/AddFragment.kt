package com.juang.jinsta2

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.juang.jinsta2.databinding.ActivityMainBinding
import com.juang.jinsta2.databinding.FragmentAddBinding

class AddFragment : Fragment() {

    private lateinit var mBinding: FragmentAddBinding
    private lateinit var mStorageReference: StorageReference
    private lateinit var mDatabaseReference: DatabaseReference

    private var mainAux: MainAux? = null

    private var mPhotoSelectedUri: Uri? = null

    private val galleryResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        if (it.resultCode == Activity.RESULT_OK) {
            mPhotoSelectedUri = it.data?.data

            with(mBinding) {
                imgPhoto.setImageURI(mPhotoSelectedUri)
                tilTitle.visibility = View.VISIBLE
                tvMessage.text = getString(R.string.post_message_valid_title)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        mBinding = FragmentAddBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupTextField()
        setupButtons()
        setupFirebase()
    }

    //Configura las referencias a Firebase Storage y Firebase Realtime Database.
    private fun setupFirebase() {
        mStorageReference = FirebaseStorage.getInstance().reference.child(JinstasApplication.PATH_JINSTAS)
        mDatabaseReference = FirebaseDatabase.getInstance().reference.child(JinstasApplication.PATH_JINSTAS)
    }

    //Abre la galería de imágenes para seleccionar una foto.
    private fun openGallery() {
        var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryResult.launch(intent)
    }

    //Publica un nuevo Jinsta, cargando la foto en Firebase Storage y guardando la información en Firebase Realtime Database.
    private fun postJinsta() {
        if(mPhotoSelectedUri != null) {
            enableUI(false)
            mBinding.progressBar.visibility = View.VISIBLE

        val key = mDatabaseReference.push().key!!
        val storageReference = mStorageReference.child(JinstasApplication.currentUser.uid)
            .child(key)

        storageReference.putFile(mPhotoSelectedUri!!)
                .addOnProgressListener {
                    val progress = (100 * it.bytesTransferred/it.totalByteCount).toInt()
                    with(mBinding) {
                        progressBar.progress = progress
                        tvMessage.text = String.format("%s%%", progress)
                    }
                }
                .addOnCompleteListener{
                        mBinding.progressBar.visibility = View.INVISIBLE
                }
                .addOnSuccessListener {
                    it.storage.downloadUrl.addOnSuccessListener { downloadUri ->
                        saveJinsta(key, downloadUri.toString(), mBinding.etTitle.text.toString().trim())
                    }
                }
                .addOnFailureListener{
                    mainAux?.showMessage(R.string.post_message_post_jinsta_fail)
                }
        }
    }

    //Habilita o deshabilita la interfaz de usuario durante el proceso de carga.
    private fun enableUI(enable: Boolean) {
        with(mBinding) {
            btnSelect.isEnabled = enable
            btnPost.isEnabled = enable
            tilTitle.isEnabled = enable
        }
    }

    //Guarda la información del Jinsta en Firebase Realtime Database y realiza acciones adicionales
    private fun saveJinsta(key: String, url: String, title: String){
        val snapshot = Jinsta(title = title, photoUrl = url)
        mDatabaseReference.child(key).setValue(snapshot)
            .addOnSuccessListener {
                hideKeyboard()
                mainAux?.showMessage(R.string.post_message_post_success)

                with(mBinding) {
                    tilTitle.visibility = View.GONE
                    etTitle.setText("")
                    tilTitle.error = null
                    tvMessage.text = getString(R.string.post_message_title)
                    imgPhoto.setImageDrawable(null)
                }
            }
            .addOnCompleteListener { enableUI(true) }
            .addOnFailureListener { mainAux?.showMessage(R.string.post_message_post_jinsta_fail) }
    }

    //Oculta el teclado virtual.
    private fun hideKeyboard() {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, 0)
    }

    //Establece la referencia a la actividad principal a través de la interfaz MainAux.
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mainAux = activity as MainAux
    }

    //Configura los campos de texto y agrega un escuchador de texto para validar los campos de entrada.
    private fun setupTextField() {
        with(mBinding) {
            etTitle.addTextChangedListener { validateFields(tilTitle) }
        }
    }

    //Configura los botones y asigna los controladores de eventos.
    private fun setupButtons() {
        with(mBinding) {
            btnPost.setOnClickListener { if (validateFields(tilTitle)) postJinsta() }
            btnSelect.setOnClickListener { openGallery() }
        }
    }

    //Valida los campos de entrada en busca de posibles errores.
    private fun validateFields(vararg textFields: TextInputLayout): Boolean {
        var isValid = true

        for (textField in textFields) {
            if (textField.editText?.text.toString().trim().isEmpty()) {
                textField.error = getString(R.string.helper_required)
                isValid = false
            } else textField.error = null
        }

        return isValid
    }
}