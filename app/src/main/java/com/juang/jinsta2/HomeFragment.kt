package com.juang.jinsta2

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.juang.jinsta2.databinding.FragmentHomeBinding
import com.juang.jinsta2.databinding.ItemJinstaBinding

class HomeFragment : Fragment() , FragmentAux{

    private lateinit var mBinding: FragmentHomeBinding

    private lateinit var mFirebaseAdapter: FirebaseRecyclerAdapter<Jinsta, JinstaHolder>
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var mDatabaseReference: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupFirebase()
        setupAdapter()
        setupRecyclerView()
    }

    //Configura la referencia de la base de datos de Firebase para acceder a los datos de los Jinstas.
    private fun setupFirebase() {
        mDatabaseReference = FirebaseDatabase.getInstance().reference.child(JinstasApplication.PATH_JINSTAS)
    }

    //Configura el adaptador FirebaseRecyclerAdapter para mostrar los Jinstas en el RecyclerView. Define los métodos para crear y vincular los elementos del RecyclerView.
    private fun setupAdapter() {
        val query = mDatabaseReference

        val options = FirebaseRecyclerOptions.Builder<Jinsta>().setQuery(query) {
            val jinsta = it.getValue(Jinsta::class.java)
            jinsta!!.id = it.key!!
            jinsta
        }.build()

        mFirebaseAdapter = object : FirebaseRecyclerAdapter<Jinsta, JinstaHolder>(options) {
            private lateinit var mContext: Context

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JinstaHolder {
                mContext = parent.context

                val view = LayoutInflater.from(mContext)
                    .inflate(R.layout.item_jinsta, parent, false)
                return JinstaHolder(view)
            }

            override fun onBindViewHolder(holder: JinstaHolder, position: Int, model: Jinsta) {
                val snapshot = getItem(position)

                with(holder) {
                    setListener(snapshot)

                    kotlin.with(binding) {
                        tvTitle.text = snapshot.title
                        cbLike.text = snapshot.likeList.keys.size.toString()
                        cbLike.isChecked = snapshot.likeList
                            .containsKey(JinstasApplication.currentUser.uid)

                        Glide.with(mContext)
                            .load(snapshot.photoUrl)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .into(imgPhoto)
                    }
                }
            }

            @SuppressLint("NotifyDataSetChanged")//error interno firebase ui 8.0.0
            override fun onDataChanged() {
                super.onDataChanged()
                mBinding.progressBar.visibility = View.GONE
                notifyDataSetChanged()
            }

            override fun onError(error: DatabaseError) {
                super.onError(error)
                //Toast.makeText(mContext, error.message, Toast.LENGTH_SHORT).show()
                Snackbar.make(mBinding.root, error.message, Snackbar.LENGTH_SHORT).show()
            }
        }
    }

    //Configura el RecyclerView con un administrador de diseño y el adaptador FirebaseRecyclerAdapter.
    private fun setupRecyclerView() {
        mLayoutManager = LinearLayoutManager(context)

        mBinding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManager
            adapter = mFirebaseAdapter
        }
    }

    //Inicia la escucha del adaptador FirebaseRecyclerAdapter para comenzar a recibir actualizaciones en tiempo real de los datos de Firebase.
    override fun onStart() {
        super.onStart()
        mFirebaseAdapter.startListening()
    }

    //Detiene la escucha del adaptador FirebaseRecyclerAdapter cuando el fragmento se detiene.
    override fun onStop() {
        super.onStop()
        mFirebaseAdapter.stopListening()
    }

    //Muestra un cuadro de diálogo de confirmación y, si se confirma, borra la foto del Jinsta de Firebase Storage
    // y elimina la entrada correspondiente de la base de datos.
    private fun deleteJinsta(jinsta: Jinsta){
        context?.let {
            MaterialAlertDialogBuilder(it)
                .setTitle(R.string.dialog_delete_title)
                .setPositiveButton(R.string.dialog_delete_confirm) { _, _ ->
                    val storageSnapshotsRef = FirebaseStorage.getInstance().reference
                        .child(JinstasApplication.PATH_JINSTAS)
                        .child(JinstasApplication.currentUser.uid)
                        .child(jinsta.id)
                    storageSnapshotsRef.delete().addOnCompleteListener { result ->
                        if (result.isSuccessful){
                            mDatabaseReference.child(jinsta.id).removeValue()
                        } else {
                            Snackbar.make(mBinding.root, getString(R.string.home_delete_photo_error),
                                Snackbar.LENGTH_LONG).show()
                        }
                    }
                }
                .setNegativeButton(R.string.dialog_delete_cancel, null)
                .show()
        }
    }

    //Establece o elimina un "me gusta" en un Jinsta, actualizando la base de datos en consecuencia.
    private fun setLike(jinsta: Jinsta, checked: Boolean){
        val myUserRef = mDatabaseReference.child(jinsta.id)
            .child(JinstasApplication.PROPERTY_LIKE_LIST)
            .child(JinstasApplication.currentUser.uid)

        if (checked) {
            myUserRef.setValue(checked)
        } else {
            myUserRef.setValue(null)
        }
    }

    //Desplaza suavemente el RecyclerView a la posición inicial, utilizado para actualizar la vista cuando se realiza una acción que afecta al conjunto de datos.
    override fun refresh() {
        mBinding.recyclerView.smoothScrollToPosition(0)
    }

    //Clase interna que representa un elemento del RecyclerView. Configura los listener para los botones de "eliminar" y "me gusta" en cada elemento del RecyclerView.
    inner class JinstaHolder(view: View) : RecyclerView.ViewHolder(view){
        val binding = ItemJinstaBinding.bind(view)

        fun setListener(jinsta: Jinsta) {
            with(binding) {
                btnDelete.setOnClickListener { deleteJinsta(jinsta) }

                cbLike.setOnCheckedChangeListener { _, checked ->
                    setLike(jinsta, checked)
                }
            }
        }
    }
}